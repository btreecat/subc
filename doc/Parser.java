import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Parser 
{
	public static void main(String[] args)
	{
		// Set up Scanner
		Scanner in = null;
		if (args.length == 1) {
			try {
				in = new Scanner(new File(args[0]));
			}
			catch (IOException e) {
				System.err.printf("Error: invalid input file\n");
				System.exit(-1);
			}
		}
		else {
				in = new Scanner(System.in);
		}
	
		// Get output for bitwise, array, and recursive
		String type;
		for (int i = 0; i < 3; i++) {
			
			String line = null;
			try {
				while ((line = in.nextLine().toLowerCase()).length() == 0);
			}
			catch (Exception e) {
				System.err.println("Error: expected more output");
				System.exit(-1);
			}
			
			// Check name of function
			type = line.substring(0, line.indexOf(":"));
			if (!(type.equals("bitwise") || type.equals("array") || type.equals("recursive"))) {
				System.err.printf("Error: Unrecognized function: %s\n", type);
				System.exit(-1);
			}
			
			// Check for no match found case
			if (line.indexOf("no match found") >= 0) {
				System.out.printf("Found no %s matches\n", type);
			}
			
			// Check for match found case; print rest of results
			else if (line.indexOf("match found") >= 0) {
				get_info(type, in);
			}
			
			// Error if unrecognized
			else {
				System.err.printf("Error: Unrecognized line: %s\n", line);
				System.exit(-1);
			}
		}
	}
	
	/** 
	 * Attempt to get the remaining two lines of input containing information
	 * about the number of matches and their positions.
	 */
	private static void get_info(String type, Scanner in)
	{
		for (int i = 0; i < 2; i++) {
			int match;
			String line;
			
			// Get next line
			while ((line = in.nextLine()).length() == 0);
			
			// Check for expected function type
			line = line.toLowerCase();
			if (line.indexOf(type) != 0) {
				System.err.printf("Error: Expected function type %s\n", type);
				System.exit(-1);
			}
			
			// Check for line containing the number of matches
			if ((match = line.indexOf("matches:")) >= 0) {
				line = line.substring(match + 8).trim();	
				System.out.printf("Found %s number of matches: %d\n", type, Integer.parseInt(line));
			}
			
			// Check for line containing all of the positions
			else if ((match = line.indexOf("positions:")) >= 0) {
				for (String s : line.substring(match + 10).trim().split(",")) {
					System.out.printf("Found %s match: %d\n", type, Integer.parseInt(s.trim()));
				}
			}
			
			// Error if unrecognized
			else {
				System.err.printf("Error: Unrecognized line: %s\n", line);
				System.exit(-1);
			}
		}
		
		System.out.println("");
	}
}
