/*
 * recursive.c
 *
 *  Created on: Feb 14, 2012
 *      Author: stanner
 *Copyright   : This file is part of subc.

                subc is free software: you can redistribute it and/or modify
                it under the terms of the GNU General Public License as published by
                the Free Software Foundation, either version 3 of the License, or
                (at your option) any later version.

                subc is distributed in the hope that it will be useful,
                but WITHOUT ANY WARRANTY; without even the implied warranty of
                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                GNU General Public License for more details.

                You should have received a copy of the GNU General Public License
                along with subc.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "substring.h"

static int SIZE = 32;

bool find_subs(bool found, int *match_count, int shifts_left, int first_int, int second_int, int mask, int firstbit);

bool recursive_find_substring (int first_int, int second_int) {
        //The main recursive function.
        //Will return true if it finds and prints a match

        int match_count = 0;
        bool found = false;
        int firstbit = 0;
        int i;


        //build a mask
        int length = 0;
        int mask = 0;
        for (i = SIZE - 1; i >= 0; i--) {
            int check = second_int & (1 << i);
            if (check) {
                //Congrats, you found a bit set to 1
                //printf("2nd bit is a 1");
                //Now set remaining bits to 1
                int j;
                for(j = i; j >= 0; j--) {
                    mask |= (1 << j);
                    length++;
                }
                break;
            }
        }
        //handle the 0 search
        if (!length && !mask) {
            length = mask = 1;
        }
        //Check first int for first bit
        for (i = SIZE - 1; i >= 0; i--) {
            int check = first_int & (1 << i);
            if (check) {
                //Congrats, you found a bit set to 1
                firstbit = i + 1;
                break;
            }
        }
        //handel 0 case
        if (!firstbit) {
            firstbit = 1;
        }

        int shifts_left = firstbit - length;
        //Handel second int too large
        if (shifts_left < 0 ) {
            return false;
        }

        //run recursive helper function
        bool sub_found = find_subs(found, &match_count, shifts_left, first_int, second_int, mask, firstbit);

        if (sub_found) {
            printf("\n\n");
            printf("Recursive: number of matches: %d\n",match_count);
            return true;
        }
        else {
            return false;
        }

}


bool find_subs(bool found, int *match_count, int shifts_left, int first_int, int second_int, int mask, int firstbit) {

    //Handle the base case
    if (shifts_left < 0) {
        if (*match_count == 0) {
            return false;
        }
        return true;
    }


    //If shifts_left > 0
    int masked = (first_int & mask);
    if (masked == second_int) {
        //matches[*match_count] = SIZE - shifts_left;
        int bit = firstbit - shifts_left;
        if (!bit) {
            bit = 1;
        }
        if (!found) {
            printf("Recursive: Match Found\n\n");
            printf("Recursive: starting bit positions: ");
            printf("%d", bit);
            found = true;
        }
        else {
            printf(",%d", bit);

        }
        *match_count = *match_count + 1;
    }
    //Regardless of finding a match, shift and recurse
    mask = (mask << 1);
    second_int = (second_int << 1);
    shifts_left--;
    return find_subs(found, match_count, shifts_left, first_int, second_int, mask, firstbit);
}
