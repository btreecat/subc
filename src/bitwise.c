/*
 * bitwise.c
 *
 *  Created on: Feb 14, 2012
 *      Author: stanner
 *Copyright   : This file is part of subc.

                subc is free software: you can redistribute it and/or modify
                it under the terms of the GNU General Public License as published by
                the Free Software Foundation, either version 3 of the License, or
                (at your option) any later version.

                subc is distributed in the hope that it will be useful,
                but WITHOUT ANY WARRANTY; without even the implied warranty of
                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                GNU General Public License for more details.

                You should have received a copy of the GNU General Public License
                along with subc.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "substring.h"

static int SIZE = 32;


bool bitwise_find_substring (int first_int, int second_int) {
    //main bitwise function.
    //Reuturns true if it finds a match

    int m_index = 0;
    bool found = false;
    int i;
    int firstbit = 0;

    int length = 0;
    int mask = 0;
    for (i = SIZE - 1; i >= 0; i--) {
        int check = second_int & (1 << i);
        if (check) {
            //Congrats, you found a bit set to 1
            //printf("2nd bit is a 1");
            //Now set remaining bits to 1
            int j;
            for(j = i; j >= 0; j--) {
                mask |= (1 << j);
                length++;
            }
            break;
        }
    }
    //handle the 0 search
    if (!length && !mask) {
        length = mask = 1;
    }
    //Check first int for first bit
    for (i = SIZE - 1; i >= 0; i--) {
        int check = first_int & (1 << i);
        if (check) {
            //Congrats, you found a bit set to 1
            firstbit = i + 1;
            break;
        }
    }
    if (!firstbit) {
        firstbit = 1;
    }
    //printf("%d", mask);
    int shift = firstbit - length;
    if (shift < 0) {
        return false;
    }
    for (i  = 0; i <= shift; i++) {

        int masked = first_int & mask;
        if (masked == second_int) {
            //Ok, so you have found a match of bits,
            //Now you need to save the position of the left most bit
            //aka (length + i)
            if (!found) {
                printf("Bitwise: Match Found\n\n");
                printf("Bitwise: starting bit positions: ");
                printf("%d", (length+i));
                found = true;
            }
            else {
                printf(",%d", (length+i));

            }
            //matches[m_index] = length + i;
            m_index++;
        }

        //Shift both mask position, and second integer.
        //This will keep the two sets of bits aligned.
        mask = (mask << 1);
        second_int = (second_int << 1);
    }
    if (found) {
                printf("\n\n");
                printf("Bitwise: number of matches: %d\n\n\n",m_index);
                return true;
    }
    else {
        return false;
    }


}

