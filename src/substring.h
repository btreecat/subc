/*
 * substring.h
 *
 *  Created on: Feb 14, 2012
 *      Author: stanner
 *Copyright   : This file is part of subc.

                subc is free software: you can redistribute it and/or modify
                it under the terms of the GNU General Public License as published by
                the Free Software Foundation, either version 3 of the License, or
                (at your option) any later version.

                subc is distributed in the hope that it will be useful,
                but WITHOUT ANY WARRANTY; without even the implied warranty of
                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                GNU General Public License for more details.

                You should have received a copy of the GNU General Public License
                along with subc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SUBSTRING_H_
#define SUBSTRING_H_

//Include these in all files
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

//Declerations of war! errr I mean functions
bool bitwise_find_substring (int first_int, int second_int);
bool array_find_substring (int first_int, int second_int);
bool recursive_find_substring (int first_int, int second_int);


#endif /* SUBSTRING_H_ */
