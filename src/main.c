/*
 ============================================================================
 Name        : main.c
 Author      : stanner
 Version     :
 Copyright   : This file is part of subc.

                subc is free software: you can redistribute it and/or modify
                it under the terms of the GNU General Public License as published by
                the Free Software Foundation, either version 3 of the License, or
                (at your option) any later version.

                subc is distributed in the hope that it will be useful,
                but WITHOUT ANY WARRANTY; without even the implied warranty of
                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                GNU General Public License for more details.

                You should have received a copy of the GNU General Public License
                along with subc.  If not, see <http://www.gnu.org/licenses/>.
 Description : Searches for binary "strings" inside other binary "strings"
 ============================================================================
 */


#include "substring.h"

int main(void) {

    printf("Welcome to subc. The program that searches for sub-binary-c-strings.\n");
    //puts("What is the first integer?: ");
    int first_int = 0;
    int second_int = 0;
    printf("What is the first integer?: ");
    scanf("%d", &first_int);
    printf("What is the second integer?: ");
    scanf("%d", &second_int);
    printf("\n");

    //Check the input
    if (first_int < 0 || second_int < 0) {
        printf("No negative numbers please... \n -_- \n");
        return 1;
    }

    //get to work
    bool bitwise = bitwise_find_substring(first_int, second_int);
    if (!bitwise) {
        printf("Bitwise: No Match Found\n\n");
    }
    bool array = array_find_substring(first_int, second_int);
    if (!array) {
        printf("Array: No Match Found\n\n");
    }
    bool recursive = recursive_find_substring(first_int, second_int);
    if (!recursive) {
            printf("Recursive: No Match Found\n\n");
    }

    return 0;
}


