/*
 * array.c
 *
 *  Created on: Feb 14, 2012
 *      Author: stanner
 *Copyright   : This file is part of subc.

                subc is free software: you can redistribute it and/or modify
                it under the terms of the GNU General Public License as published by
                the Free Software Foundation, either version 3 of the License, or
                (at your option) any later version.

                subc is distributed in the hope that it will be useful,
                but WITHOUT ANY WARRANTY; without even the implied warranty of
                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                GNU General Public License for more details.

                You should have received a copy of the GNU General Public License
                along with subc.  If not, see <http://www.gnu.org/licenses/>.
 */
//Imports
#include "substring.h"

//Global Vars
static int SIZE = 32;

//Function prototypes
void i_to_bin(int n, char n_array[]);
bool compare_bins (char orig[], char comp[]);

bool array_find_substring (int first_int, int second_int) {
    /*
     * This method is used to return true or false based on the
     * comparison of two substrings. If true, at least
     * one match has been found.
     */

    //Place holder variables
    char n1_array[SIZE];
    char n2_array[SIZE];
    char comp_array[SIZE];
    int matches[SIZE];
    int match_cnt = 0;
    int firstbit = 0;

    //For for loops
    int i;

    for (i = 0; i < SIZE; i++) {
        n1_array[i] = '0';
        n2_array[i] = '0';
        comp_array[i] = '0';
        matches[i] = 0;
    }

    //convert the input integers to arrays
    i_to_bin(first_int, n1_array);
    i_to_bin(second_int, n2_array);

    //get the length of the sub string
    int length = 0;
    for (i = SIZE -1; i >= 0; i--) {

        if (n2_array[i] == '1') {
            length = i + 1;
            break;
        }
    }
    //handel 0 search
    if (!length) {
        length = 1;
    }
    //get first bit
    for (i = SIZE -1; i >= 0; i--) {
        if (n1_array[i] == '1') {
            firstbit = i + 1;
            break;
        }
    }
    if (!firstbit) {
        firstbit = 1;
    }

    //Calculate how many shifts are left
    int moves = firstbit - length;

    if (moves < 0) {
        return false;
    }


    //For each shift...
    for (i = 0; i <= moves; i ++) {
        int j;
        //Populate the comp_array for comparing;
        for (j = 0; j < length; j++) {
            comp_array[j] = n1_array[i+j];
        }
        bool match = compare_bins(n2_array, comp_array);
        if (match) {
            matches[match_cnt] = i + length;
            match_cnt++;
        }


    }
    //Make sure we found at least one
    if (match_cnt == 0) {
        return false;
    }
    //Since we found at least one, print the results
    printf("Array: Match Found\n\n");
    printf("Array: starting bit positions: ");
    for (i = 0; i < SIZE; i++) {
        if (!matches[i]) {
            break;
        }
        printf("%d", matches[i]);
        if (matches[i + 1]) {
            printf(",");
        }
    }
    printf("\n\n");
    printf("Array: number of matches: %d\n\n\n",match_cnt);
    return true;
}

bool compare_bins (char orig[], char comp[]) {
    //This method is used to compare two binary-char arrays
    int i;
    for (i = 0; i < SIZE; i++) {
        if (orig[i] != comp[i]) {
            return false;
        }
    }

    return true;
}


void i_to_bin(int n, char n_array[]) {
    //This method is used to convert any 32bit number to a bin-char array
    int i;
    for (i = 0; i < SIZE; i++) {
        if (n & (1 << i)) {
            n_array[i] = '1';
        }
    }
}
